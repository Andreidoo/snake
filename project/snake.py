import pygame
import sys
import random
import os
from pygame.math import Vector2

# Classes
class Fruit:
    def __init__(self):
        self.x = random.randint(0, cell_number - 1)
        self.y = random.randint(0, cell_number - 1)
        self.pos = Vector2(self.x, self.y)

    def draw_fruit(self):
        x_pos = int(self.pos.x * cell_size)
        y_pos = int(self.pos.y * cell_size)
        fruit_rect = pygame.Rect(x_pos, y_pos, cell_size, cell_size)
        screen.blit(apple, fruit_rect)

    def randomize(self):
        self.x = random.randint(0, cell_number - 1)
        self.y = random.randint(0, cell_number - 1)
        self.pos = Vector2(self.x, self.y)

class Snake:
    def __init__(self):
        self.body = [Vector2(5, 10), Vector2(4, 10), Vector2(3, 10)]
        self.direction = Vector2(1, 0)
        self.new_block = False

        self.head_1 = load_image("snake/head/head_1.png").convert_alpha()
        self.head_2 = load_image("snake/head/head_2.png").convert_alpha()
        self.head_3 = load_image("snake/head/head_3.png").convert_alpha()
        self.head_4 = load_image("snake/head/head_4.png").convert_alpha()

        self.tail_1 = load_image("snake/tail/tail_1.png").convert_alpha()
        self.tail_2 = load_image("snake/tail/tail_2.png").convert_alpha()
        self.tail_3 = load_image("snake/tail/tail_3.png").convert_alpha()
        self.tail_4 = load_image("snake/tail/tail_4.png").convert_alpha()

        self.body_1 = load_image("snake/body/body_1.png").convert_alpha()
        self.body_2 = load_image("snake/body/body_2.png").convert_alpha()

        self.curved_1 = load_image("snake/curved/curved_1.png").convert_alpha()
        self.curved_2 = load_image("snake/curved/curved_2.png").convert_alpha()
        self.curved_3 = load_image("snake/curved/curved_3.png").convert_alpha()
        self.curved_4 = load_image("snake/curved/curved_4.png").convert_alpha()

        self.sfx_eat = load_sfx("sounds/sfx_eat.wav")
    
    def draw_snake(self):
        self.update_head_graphics()
        self.update_tail_graphics()

        for index, block in enumerate(self.body):
            x_pos = int(block.x * cell_size)
            y_pos = int(block.y * cell_size)
            block_rect = pygame.Rect(x_pos, y_pos, cell_size, cell_size)

            if index == 0:
                screen.blit(self.head, block_rect)
            elif index == len(self.body) - 1:
                screen.blit(self.tail, block_rect)
            else:
                previous_block = self.body[index + 1] - block
                next_block = self.body[index - 1] - block
                if previous_block.x == next_block.x:
                    screen.blit(self.body_2, block_rect)
                elif previous_block.y == next_block.y:
                    screen.blit(self.body_1, block_rect)
                else:
                    if previous_block.x == -1 and next_block.y == -1 or previous_block.y == -1 and next_block.x == -1:
                        screen.blit(self.curved_4, block_rect)
                    elif previous_block.x == -1 and next_block.y == 1 or previous_block.y == 1 and next_block.x == -1:
                        screen.blit(self.curved_1, block_rect)
                    elif previous_block.x == 1 and next_block.y == -1 or previous_block.y == -1 and next_block.x == 1:
                        screen.blit(self.curved_3, block_rect)
                    elif previous_block.x == 1 and next_block.y == 1 or previous_block.y == 1 and next_block.x == 1:
                        screen.blit(self.curved_2, block_rect)

    def update_head_graphics(self):
        head_relation = self.body[1] - self.body[0]
        if head_relation == Vector2(1, 0):
            self.head = self.head_3
        elif head_relation == Vector2(-1, 0):
            self.head = self.head_2
        elif head_relation == Vector2(0, 1):
            self.head = self.head_1
        elif head_relation == Vector2(0, -1):
            self.head = self.head_4

    def update_tail_graphics(self):
        tail_relation = self.body[-2] - self.body[-1]
        if tail_relation == Vector2(1, 0):
            self.tail = self.tail_3
        elif tail_relation == Vector2(-1, 0):
            self.tail = self.tail_2
        elif tail_relation == Vector2(0, 1):
            self.tail = self.tail_1
        elif tail_relation == Vector2(0, -1):
            self.tail = self.tail_4

    def move_snake(self):
        if self.new_block == True:
            body_copy = self.body[:]
            body_copy.insert(0, body_copy[0] + self.direction)
            self.body = body_copy[:]
            self.new_block = False
        else:
            body_copy = self.body[:-1]
            body_copy.insert(0, body_copy[0] + self.direction)
            self.body = body_copy[:]

    def add_block(self):
        self.new_block = True
    
    def play_eat_sound(self):
        self.sfx_eat.play()
    
    def reset(self):
        self.body = [Vector2(5, 10), Vector2(4, 10), Vector2(3, 10)]
        self.direction = Vector2(0, 0)

class Main:
    def __init__(self):
        self.snake = Snake()
        self.fruit = Fruit()

    def update(self):
        self.snake.move_snake()
        self.check_collision()
        self.check_fail()

    def draw_elements(self):
        self.draw_grass()
        self.fruit.draw_fruit()
        self.snake.draw_snake()
        self.draw_score()

    def check_collision(self):
        if self.fruit.pos == self.snake.body[0]:
            self.fruit.randomize()
            self.snake.add_block()
            self.snake.play_eat_sound()

        for block in self.snake.body[1:]:
            if block == self.fruit.pos:
                self.fruit.randomize()

    def check_fail(self):
        if not 0 <= self.snake.body[0].x < cell_number or not 0 <= self.snake.body[0].y < cell_number:
            self.game_over()

        for block in self.snake.body[1:]:
            if block == self.snake.body[0]:
                self.game_over()

    def game_over(self):
        self.snake.reset()

    def draw_grass(self):
        grass_color = (167, 209, 61)
        for row in range(cell_number):
            if row % 2 == 0:
                for col in range(cell_number):
                    if col % 2 == 0:
                        grass_rect = pygame.Rect(col * cell_size, row * cell_size, cell_size, cell_size)
                        pygame.draw.rect(screen, grass_color, grass_rect)
            else:
                for col in range(cell_number):
                    if col % 2 != 0:
                        grass_rect = pygame.Rect(col * cell_size, row * cell_size, cell_size, cell_size)
                        pygame.draw.rect(screen, grass_color, grass_rect)

    def draw_score(self):
        score_text = str(len(self.snake.body) - 3)
        score_surface = game_font.render(score_text, True, (56, 74, 12))
        score_x = int(cell_size * cell_number - 20)
        score_y = int(cell_size * cell_number - 20)
        score_rect = score_surface.get_rect(center = (score_x, score_y))
        apple_rect = apple.get_rect(midright = (score_rect.left, score_rect.centery))

        screen.blit(score_surface, score_rect)
        screen.blit(apple, apple_rect)

# Functions
def load_image(filename):
    internal_path = os.path.join("_internal", filename)
    src_path = os.path.join("src", filename)
    if os.path.exists(internal_path):
        return pygame.image.load(internal_path)
    elif os.path.exists(src_path):
        return pygame.image.load(src_path)

# Load custom font
if os.path.exists(os.path.join("_internal", "fonts", "Snake Chan.ttf")):
    font_path = os.path.join("_internal", "fonts", "Snake Chan.ttf")
elif os.path.exists(os.path.join("fonts", "Snake Chan.ttf")):
    font_path = os.path.join("fonts", "Snake Chan.ttf")

# Load sounds
def load_sfx(filename):
    internal_path = os.path.join("_internal", filename)
    src_path = os.path.join("src", filename)
    if os.path.exists(internal_path):
        return pygame.mixer.Sound(internal_path)
    elif os.path.exists(src_path):
        return pygame.mixer.Sound(src_path)

# Init
pygame.mixer.pre_init(44100, -16, 2, 512)
pygame.init()

# Icon
icon_size = (32, 32)
icon_path = load_image("icons/icon.png")
icon_image = pygame.transform.scale(icon_path, icon_size)
pygame.display.set_icon(icon_image)

# Global variables
cell_size = 30
cell_number = 20
screen = pygame.display.set_mode((cell_number * cell_size, cell_number * cell_size))
clock = pygame.time.Clock()
game_font = pygame.font.Font(font_path, 30)
pygame.display.set_caption("Snake")

# Images
apple = load_image("apple/apple.png").convert_alpha()

# Timer
SCREEN_UPDATE = pygame.USEREVENT
pygame.time.set_timer(SCREEN_UPDATE, 150)

# Main
main_game = Main()

# Main loop
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == SCREEN_UPDATE:
            main_game.update()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                if main_game.snake.direction.y != 1:
                    main_game.snake.direction = Vector2(0, -1)
            if event.key == pygame.K_RIGHT:
                if main_game.snake.direction.x != -1:
                    main_game.snake.direction = Vector2(1, 0)
            if event.key == pygame.K_DOWN:
                if main_game.snake.direction.y != -1:
                    main_game.snake.direction = Vector2(0, 1)
            if event.key == pygame.K_LEFT:
                if main_game.snake.direction.x != 1:
                    main_game.snake.direction = Vector2(-1, 0)
    
    screen.fill((175, 215, 70))
    main_game.draw_elements()
    pygame.display.update()
    clock.tick(60)